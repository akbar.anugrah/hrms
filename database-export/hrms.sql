-- MySQL dump 10.13  Distrib 8.0.34, for Win64 (x86_64)
--
-- Host: localhost    Database: hrms
-- ------------------------------------------------------
-- Server version	5.7.44-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `account` (
  `account_code` varchar(45) NOT NULL,
  `account_name` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `number_of_emp` int(11) DEFAULT NULL,
  `account_contact` varchar(100) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`account_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `company` (
  `company_code` varchar(45) NOT NULL,
  `company_name` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`company_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_no` varchar(45) NOT NULL,
  `employee_name` varchar(150) NOT NULL,
  `emp_type` tinyint(1) DEFAULT NULL,
  `sex` char(1) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `jobtitle_code` varchar(45) DEFAULT NULL,
  `account_code` varchar(45) DEFAULT NULL,
  `company_code` varchar(45) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_index` (`employee_no`,`sex`,`email`,`username`,`password`,`jobtitle_code`,`company_code`,`account_code`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'EMP001','Admin',1,'m','admin@admin.com','admin','812a69f312a8b9a75812761f8dfeb8b0','ADMIN','mekari','talenta','2024-06-03 23:23:19','admin','admin','2024-06-03 23:23:19'),(3,'EMP002','Budi Susanto',2,'m','mekr@mekr.com','budi','d41d8cd98f00b204e9800998ecf8427e','STAFF_QC','mekari','talenta','2024-06-05 22:44:13','admin','admin','2024-06-05 22:44:13'),(4,'EMP003','Joko Santoso',2,'m','mekr@mekr.com','joko','d41d8cd98f00b204e9800998ecf8427e','SPV_QC','mekari','talenta','2024-06-05 22:47:40','admin','admin','2024-06-05 22:47:40'),(5,'EMP003','Budi Gunawan',2,'m','mekr@mekr.com','gunawan','d41d8cd98f00b204e9800998ecf8427e','MGR_QC','mekari','talenta','2024-06-05 22:48:05','admin','admin','2024-06-05 22:48:05');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobtitle`
--

DROP TABLE IF EXISTS `jobtitle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jobtitle` (
  `jobtitle_code` varchar(45) NOT NULL,
  `jobtitle_name` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `company_code` varchar(45) DEFAULT NULL,
  `account_code` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`jobtitle_code`),
  KEY `jobtitle_index` (`jobtitle_code`,`company_code`,`account_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobtitle`
--

LOCK TABLES `jobtitle` WRITE;
/*!40000 ALTER TABLE `jobtitle` DISABLE KEYS */;
INSERT INTO `jobtitle` VALUES ('ADMIN','Admin 17an','Admin of HRIS','2024-06-03 23:43:07','admin','2024-06-03 23:43:07','admin','talenta','mekari'),('MGR_QC','Manager QC Employee','Manager QC Employee','2024-06-03 23:43:07','admin','2024-06-03 23:43:07','admin','talenta','mekari'),('SPV_QC','Supervisor QC Employee','Staff QC Employee','2024-06-03 23:43:07','admin','2024-06-03 23:43:07','admin','talenta','mekari'),('STAFF_QC','Staff QC Employee','Staff QC Employee','2024-06-03 23:43:07','admin','2024-06-03 23:43:07','admin','talenta','mekari');
/*!40000 ALTER TABLE `jobtitle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leave_request`
--

DROP TABLE IF EXISTS `leave_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `leave_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `leave_request_no` varchar(45) DEFAULT NULL,
  `company_code` varchar(45) DEFAULT NULL,
  `account_code` varchar(45) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `leave_start` datetime DEFAULT NULL,
  `leave_end` datetime DEFAULT NULL,
  `request_status` tinyint(2) DEFAULT NULL,
  `leave_reason` varchar(255) DEFAULT NULL,
  `leave_type` tinyint(2) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `leave_req_index` (`company_code`,`account_code`,`employee_id`,`request_status`,`leave_type`,`leave_request_no`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leave_request`
--

LOCK TABLES `leave_request` WRITE;
/*!40000 ALTER TABLE `leave_request` DISABLE KEYS */;
INSERT INTO `leave_request` VALUES (5,'LVREQ-3-5','talenta','mekari',3,'2024-09-09 00:00:00','2024-09-10 00:00:00',2,'holiday',1,'2024-06-07 01:23:12','budi','2024-06-07 06:50:21','gunawan');
/*!40000 ALTER TABLE `leave_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `org_structure`
--

DROP TABLE IF EXISTS `org_structure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `org_structure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_code` varchar(45) DEFAULT NULL,
  `account_code` varchar(45) DEFAULT NULL,
  `parent_emp_id` int(11) DEFAULT NULL,
  `child_emp_id` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `org_struct_index` (`company_code`,`account_code`,`parent_emp_id`,`child_emp_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `org_structure`
--

LOCK TABLES `org_structure` WRITE;
/*!40000 ALTER TABLE `org_structure` DISABLE KEYS */;
INSERT INTO `org_structure` VALUES (1,'talenta','mekari',5,4,'2024-06-06 05:50:04','admin','2024-06-06 05:50:04','admin'),(2,'talenta','mekari',4,3,'2024-06-06 05:50:04','admin','2024-06-06 05:50:04','admin');
/*!40000 ALTER TABLE `org_structure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `request_approval`
--

DROP TABLE IF EXISTS `request_approval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `request_approval` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) DEFAULT NULL,
  `request_type` varchar(45) DEFAULT NULL,
  `account_code` varchar(45) DEFAULT NULL,
  `company_code` varchar(45) DEFAULT NULL,
  `approver_emp_id` int(11) DEFAULT NULL,
  `sequence` tinyint(2) DEFAULT NULL,
  `approval_status` tinyint(1) DEFAULT NULL,
  `done` tinyint(1) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `request_approval_index` (`request_id`,`account_code`,`company_code`,`approver_emp_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `request_approval`
--

LOCK TABLES `request_approval` WRITE;
/*!40000 ALTER TABLE `request_approval` DISABLE KEYS */;
INSERT INTO `request_approval` VALUES (7,5,'LEAVE','mekari','talenta',4,0,1,1,'2024-06-07 06:47:22','joko','2024-06-07 01:23:12','budi'),(8,5,'LEAVE','mekari','talenta',5,1,1,1,'2024-06-07 06:50:21','gunawan','2024-06-07 01:23:12','budi');
/*!40000 ALTER TABLE `request_approval` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-06-07 14:02:03
