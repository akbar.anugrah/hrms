
# API CRUD HRMS (Human Resource Information System) 

## Business Requirement
Employee Management
- Add new employees.
- List all employees.
- Edit employee details.
- Delete employees.
- Leave Management

Allow employees to apply for leave.
- Form request time off
- Approve or reject leave applications.

You could see the implementation of the above requirements in Go in [this folder](https://gitlab.com/akbar.anugrah/hrms/-/tree/main/src/entity?ref_type=heads).

Also, you could see the API demo in action in [this folder](https://gitlab.com/akbar.anugrah/hrms/-/tree/main/postman?ref_type=heads).

## Database Migration
ERD Design and Database exported structure could be found in [this folder](https://gitlab.com/akbar.anugrah/hrms/-/tree/main/database-export?ref_type=heads).

## Authentication
The system use JWT token for Authentication. For example Users (Staff, SPV & MGR) would have different pre-built JWT Token you can found each Postman API request. 

You could see it's Go implementation in the [http request's middleware](https://gitlab.com/akbar.anugrah/hrms/-/blob/main/src/utility/middleware.go?ref_type=heads).

## Logging
The system prints log (error & info) for any catched error & optional info. It will be formatted as JSON. You could see it's implementation in the [logger](https://gitlab.com/akbar.anugrah/hrms/-/blob/main/src/utility/logger.go?ref_type=heads).

## Design Patter
The system also implements some of the well known design pattern, such as Singleton and Facade. 

### Singleton
You could see Singleton implementation in the [database initialization](https://gitlab.com/akbar.anugrah/hrms/-/blob/main/src/utility/database.go?ref_type=heads) in [main.go](https://gitlab.com/akbar.anugrah/hrms/-/blob/main/main.go?ref_type=heads). 

### Facade
The could see Facade implementation in the [logger](https://gitlab.com/akbar.anugrah/hrms/-/blob/main/src/utility/logger.go?ref_type=heads) util. The logger will simplify the utilization of logging library.

## System Installation

### System Requirement
- Golang +1.16
- MySQL 5.7.xx

### Step by Step Installation
- [Download & Install](https://go.dev/doc/install) Golang +1.16 
- [Download & Install](https://dev.mysql.com/downloads/windows/installer/5.7.html) MySQL 5.7.xx
- Create an empty database & import a database dump that has been attached along with this system. Please adjust the API's database configuration in the config.json based on your database config (host, port, username, password, dbname, etc)
- Install this system by build & run the API source code (in Golang). Go to /api root folder and run `go mod vendor` to download all the dependencies needed. 
- Run the API source code by running `go build && /hrms.exe` (for windows) or `go build && ./hrms` (for linux). You will see message `listening on port :9999` in terminal that indicates the API is running.
- Test / Try the API by using a Postman Collection that has been attached [in this folder](https://go.dev/doc/install)

### Try it
To use / try these APIs, you could use a [Postman](https://www.postman.com/downloads/) Collection that has been attached along with this system.