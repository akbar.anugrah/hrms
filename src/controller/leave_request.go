package controller

import (
	"net/http"

	"mekr/src/api"
	"mekr/src/entity"
	"mekr/src/repository"
	"mekr/src/utility"
)

type LeaveRequestCtrlIface interface {
	CreateLeaveRequest(w http.ResponseWriter, r *http.Request)
	ListLeaveRequest(w http.ResponseWriter, r *http.Request)
	UpdateLeaveRequest(w http.ResponseWriter, r *http.Request)
}

type LeaveRequestCtrl struct {
	LeaveReqRepo        repository.LeaveRequestRepoIface
	OrgStructRepo       repository.OrgStructureRepoIface
	EmployeeRepo        repository.EmployeeRepoIface
	RequestApprovalRepo repository.RequestApprovalRepoIface
}

func InitLeaveRequestCtrl(
	leaveReqRepo repository.LeaveRequestRepoIface,
	orgStructRepo repository.OrgStructureRepoIface,
	employeeRepo repository.EmployeeRepoIface,
	requestApprovalRepo repository.RequestApprovalRepoIface) LeaveRequestCtrlIface {
	return &LeaveRequestCtrl{
		LeaveReqRepo:        leaveReqRepo,
		OrgStructRepo:       orgStructRepo,
		EmployeeRepo:        employeeRepo,
		RequestApprovalRepo: requestApprovalRepo,
	}
}

func (i *LeaveRequestCtrl) ListLeaveRequest(w http.ResponseWriter, r *http.Request) {
	sessionEmpID, _ := r.Context().Value("emp_id").(int)
	actor, err := i.EmployeeRepo.DetailEmployee(r.Context(), sessionEmpID, "")
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	if actor.ID == 0 {
		utility.ResponseError401(w, utility.Unathorized)
		return
	}

	repoParam := repository.ParamListLeaveRequest{
		EmpID:       sessionEmpID,
		AccountCode: actor.AccountCode,
		CompanyCode: actor.CompanyCode,
	}
	leaveRequests, err := i.LeaveReqRepo.List(r.Context(), repoParam)
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	leaveRequestIDs := []int{}
	for _, lReq := range leaveRequests {
		leaveRequestIDs = append(leaveRequestIDs, lReq.ID)
	}

	repoParam2 := repository.ParamListRequestApproval{
		RequestID:   leaveRequestIDs,
		AccountCode: actor.AccountCode,
		CompanyCode: actor.CompanyCode,
	}
	requestApprovals, err := i.RequestApprovalRepo.List(r.Context(), repoParam2)
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	mapLeaveRequests := make(map[int][]entity.RequestApproval)
	for _, reqAppr := range requestApprovals {
		mapLeaveRequests[reqAppr.RequestID] = append(mapLeaveRequests[reqAppr.RequestID], reqAppr)
	}

	resp := []api.GetLeaveRequest{}
	for _, leaveRequest := range leaveRequests {
		resp = append(resp, *api.ListLeaveRequests(&leaveRequest, mapLeaveRequests[leaveRequest.ID]))
	}

	utility.ResponseSuccess200(w, resp, utility.SuccessRetrieveData)
}

func (i *LeaveRequestCtrl) CreateLeaveRequest(w http.ResponseWriter, r *http.Request) {
	var err error
	body := api.CreateLeaveRequest{}
	err = utility.DecodeJsonRequest(r, &body)
	if err != nil {
		utility.ResponseError400(w, utility.MissingRequiredParam)
		return
	}

	sessionEmpID, _ := r.Context().Value("emp_id").(int)
	actor, err := i.EmployeeRepo.DetailEmployee(r.Context(), sessionEmpID, "")
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	if actor.ID == 0 {
		utility.ResponseError401(w, utility.Unathorized)
		return
	}

	// Get Org-Struct Tree
	childEmpID := actor.ID
	orgStructs := []entity.OrgStructure{}
	for {
		orgStruct, err := i.OrgStructRepo.DetailOrgStructure(r.Context(), childEmpID, 0, actor.AccountCode, actor.CompanyCode)
		if err != nil {
			utility.ResponseError500(w)
			return
		}
		if orgStruct == nil || orgStruct.ID == 0 {
			break
		}
		orgStructs = append(orgStructs, *orgStruct)
		childEmpID = *orgStruct.ParentEmpID
	}

	repoParam := repository.ParamListLeaveRequest{
		EmpID:       sessionEmpID,
		AccountCode: actor.AccountCode,
		CompanyCode: actor.CompanyCode,
	}
	prevLeaveRequests, err := i.LeaveReqRepo.List(r.Context(), repoParam)
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	leaveRequest, err := entity.CreateLeaveRequest(body.ToEntity(), actor, prevLeaveRequests)
	if err != nil {
		utility.ResponseError400(w, err)
		return
	}

	tx, err := i.LeaveReqRepo.BeginTx(r.Context())
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}
		tx.Commit()
	}()

	err = i.LeaveReqRepo.Insert(r.Context(), tx, leaveRequest)
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	leaveRequest.GenerateLeaveReqNo()

	err = i.LeaveReqRepo.Update(r.Context(), tx, leaveRequest)
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	approvals := entity.CreateRequestApproval(actor, leaveRequest, orgStructs)

	for _, approval := range approvals {
		err = i.RequestApprovalRepo.Insert(r.Context(), tx, &approval)
		if err != nil {
			utility.ResponseError500(w)
			return
		}
	}

	utility.ResponseSuccess200(w, nil, utility.SuccessSaveData)

	return
}

func (i *LeaveRequestCtrl) UpdateLeaveRequest(w http.ResponseWriter, r *http.Request) {
	var err error
	body := api.UpdateLeaveRequest{}
	err = utility.DecodeJsonRequest(r, &body)
	if err != nil {
		utility.ResponseError400(w, utility.MissingRequiredParam)
		return
	}

	sessionEmpID, _ := r.Context().Value("emp_id").(int)
	actor, err := i.EmployeeRepo.DetailEmployee(r.Context(), sessionEmpID, "")
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	if actor.ID == 0 {
		utility.ResponseError401(w, utility.Unathorized)
		return
	}

	repoParam := repository.ParamListLeaveRequest{
		LeaveRequestNo: body.LeaveRequestNo,
		ApproverID:     actor.ID,
		AccountCode:    actor.AccountCode,
		CompanyCode:    actor.CompanyCode,
	}
	leaveRequests, err := i.LeaveReqRepo.List(r.Context(), repoParam)
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	if len(leaveRequests) == 0 {
		utility.ResponseError400(w, utility.MissingRequiredParam)
		return
	}

	leaveRequestIDs := []int{}
	for _, lReq := range leaveRequests {
		leaveRequestIDs = append(leaveRequestIDs, lReq.ID)
	}

	repoParam2 := repository.ParamListRequestApproval{
		RequestID:   leaveRequestIDs,
		AccountCode: actor.AccountCode,
		CompanyCode: actor.CompanyCode,
	}
	requestApprovals, err := i.RequestApprovalRepo.List(r.Context(), repoParam2)
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	updateReqApproval, err := leaveRequests[0].UpdateStatus(body.ApprovalStatus, actor, requestApprovals)
	if err != nil {
		utility.ResponseError400(w, err)
		return
	}

	tx, err := i.LeaveReqRepo.BeginTx(r.Context())
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}
		tx.Commit()
	}()

	err = i.LeaveReqRepo.Update(r.Context(), tx, &leaveRequests[0])
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	err = i.RequestApprovalRepo.Update(r.Context(), tx, &updateReqApproval)
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	utility.ResponseSuccess200(w, nil, utility.SuccessSaveData)
}
