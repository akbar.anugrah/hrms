package controller

import (
	"mekr/src/api"
	"mekr/src/entity"
	"mekr/src/repository"
	"mekr/src/utility"
	"net/http"
)

type EmployeeCtrlIface interface {
	CreateEmployee(w http.ResponseWriter, r *http.Request)
	UpdateEmployee(w http.ResponseWriter, r *http.Request)
	ListEmployee(w http.ResponseWriter, r *http.Request)
	DeleteEmployee(w http.ResponseWriter, r *http.Request)
}

type EmployeeCtrl struct {
	EmployeeRepo repository.EmployeeRepoIface
	JobtitleRepo repository.JobtitleRepoIface
}

func InitEmployeeCtrl(
	employeeRepo repository.EmployeeRepoIface,
	jobtitleRepoo repository.JobtitleRepoIface) EmployeeCtrlIface {
	return &EmployeeCtrl{
		EmployeeRepo: employeeRepo,
		JobtitleRepo: jobtitleRepoo,
	}
}

func (i *EmployeeCtrl) ListEmployee(w http.ResponseWriter, r *http.Request) {
	var err error
	body := utility.Pagination{}
	err = utility.DecodeJsonRequest(r, &body)
	if err != nil {
		utility.ResponseError400(w, utility.MissingRequiredParam)
		return
	}

	sessionEmpID, _ := r.Context().Value("emp_id").(int)
	actor, err := i.EmployeeRepo.DetailEmployee(r.Context(), sessionEmpID, "")
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	if !actor.IsAdmin(nil) {
		utility.ResponseError401(w, utility.Unathorized)
		return
	}

	employees, err := i.EmployeeRepo.List(r.Context(), *body.ParseFromBody())
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	resp := []interface{}{}
	for _, emp := range employees {
		resp = append(resp, api.GetEmployee(emp))
	}
	utility.ResponseSuccess200(w, resp, utility.SuccessRetrieveData)
}

func (i *EmployeeCtrl) CreateEmployee(w http.ResponseWriter, r *http.Request) {
	var err error
	body := api.CreateEmployeeReq{}
	err = utility.DecodeJsonRequest(r, &body)
	if err != nil {
		utility.ResponseError400(w, utility.MissingRequiredParam)
		return
	}

	sessionEmpID, _ := r.Context().Value("emp_id").(int)

	duplicateEmp, err := i.EmployeeRepo.DetailEmployee(r.Context(), 0, body.Username)
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	actor, err := i.EmployeeRepo.DetailEmployee(r.Context(), sessionEmpID, "")
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	jobtile, err := i.JobtitleRepo.DetailJobtitle(r.Context(), body.JobtitleCode, actor.AccountCode, actor.CompanyCode)
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	emp, err := entity.CreateEmployee(body.ToEntity(), actor, jobtile, duplicateEmp)
	if err != nil {
		utility.ResponseError400(w, err)
		return
	}

	err = i.EmployeeRepo.Insert(r.Context(), nil, emp)
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	utility.ResponseSuccess200(w, nil, utility.SuccessRetrieveData)
}

func (i *EmployeeCtrl) UpdateEmployee(w http.ResponseWriter, r *http.Request) {
	var err error
	body := api.UpdateEmployeeReq{}
	err = utility.DecodeJsonRequest(r, &body)
	if err != nil {
		utility.ResponseError400(w, utility.MissingRequiredParam)
		return
	}

	sessionEmpID, _ := r.Context().Value("emp_id").(int)

	employee, err := i.EmployeeRepo.DetailEmployee(r.Context(), body.ID, "")
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	actor, err := i.EmployeeRepo.DetailEmployee(r.Context(), sessionEmpID, "")
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	jobtile, err := i.JobtitleRepo.DetailJobtitle(r.Context(), body.JobtitleCode, actor.AccountCode, actor.CompanyCode)
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	err = employee.Update(body.ToEntity(), actor, jobtile)
	if err != nil {
		utility.ResponseError400(w, err)
		return
	}

	err = i.EmployeeRepo.Update(r.Context(), nil, employee)
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	utility.ResponseSuccess200(w, nil, utility.SuccessRetrieveData)
}

func (i *EmployeeCtrl) DeleteEmployee(w http.ResponseWriter, r *http.Request) {
	var err error
	body := api.DeleteEmployeeReq{}
	err = utility.DecodeJsonRequest(r, &body)
	if err != nil {
		utility.ResponseError400(w, utility.MissingRequiredParam)
		return
	}

	sessionEmpID, _ := r.Context().Value("emp_id").(int)
	actor, err := i.EmployeeRepo.DetailEmployee(r.Context(), sessionEmpID, "")
	if err != nil {
		utility.ResponseError500(w)
		return
	}

	if !actor.IsAdmin(nil) {
		utility.ResponseError401(w, utility.Unathorized)
		return
	}

	err = i.EmployeeRepo.Delete(r.Context(), body.ID)
	if err != nil {
		utility.ResponseError500(w)
		return
	}
	utility.ResponseSuccess200(w, nil, utility.SuccessDeleteData)
}
