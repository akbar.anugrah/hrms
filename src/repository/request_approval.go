package repository

import (
	"context"
	"database/sql"
	"mekr/src/entity"
	"mekr/src/utility"

	"github.com/doug-martin/goqu/v9"
)

type RequestApprovalRepo struct {
	db *sql.DB
}

func InitRequestApprovalRepo(db *sql.DB) RequestApprovalRepoIface {
	return &RequestApprovalRepo{
		db: db,
	}
}

type RequestApprovalRepoIface interface {
	List(ctx context.Context, param ParamListRequestApproval) (list []entity.RequestApproval, err error)
	Insert(ctx context.Context, tx *sql.Tx, requestApproval *entity.RequestApproval) (err error)
	Update(ctx context.Context, tx *sql.Tx, requestApproval *entity.RequestApproval) (err error)
}

type ParamListRequestApproval struct {
	RequestID     []int
	ApproverEmpID int
	AccountCode   string
	CompanyCode   string
}

func (e *RequestApprovalRepo) List(ctx context.Context, param ParamListRequestApproval) (list []entity.RequestApproval, err error) {
	dataset := utility.SqlDialect.
		Select(
			goqu.I("rp.id"),
			goqu.I("rp.request_id"),
			goqu.I("rp.approver_emp_id"),
			goqu.I("e.employee_name"),
			goqu.I("rp.sequence"),
			goqu.I("rp.approval_status"),
			goqu.I("rp.done"),
			goqu.I("rp.account_code"),
			goqu.I("rp.company_code"),
			goqu.I("rp.modified_date"),
			goqu.I("rp.modified_by"),
			goqu.I("rp.created_date"),
			goqu.I("rp.created_by"),
		).
		From(goqu.T("request_approval").As("rp")).
		LeftJoin(goqu.T("employee").As("e"),
			goqu.On(
				goqu.I("e.id").Eq(goqu.I("rp.approver_emp_id")),
				goqu.I("e.account_code").Eq(goqu.I("rp.account_code")),
				goqu.I("e.company_code").Eq(goqu.I("rp.company_code")),
			),
		).
		Where(
			goqu.I("rp.account_code").Eq(param.AccountCode),
			goqu.I("rp.company_code").Eq(param.CompanyCode),
		).
		Order(
			goqu.I("rp.id").Asc(),
		)

	if len(param.RequestID) > 0 {
		dataset = dataset.Where(
			goqu.I("rp.request_id").In(param.RequestID),
		)
	}

	if param.ApproverEmpID > 0 {
		dataset = dataset.Where(
			goqu.I("rp.approver_emp_id").Eq(param.ApproverEmpID),
		)
	}

	query, params, err := dataset.Prepared(true).ToSQL()
	if err != nil {
		utility.Error(err, nil)
		return
	}

	rows, err := e.db.QueryContext(ctx, query, params...)
	if err != nil && err != sql.ErrNoRows {
		utility.Error(err, nil)
		return
	}
	defer rows.Close()

	for rows.Next() {
		var lr entity.RequestApproval
		if err = rows.Scan(
			&lr.ID,
			&lr.RequestID,
			&lr.ApproverEmpID,
			&lr.ApproverEmpName,
			&lr.Sequence,
			&lr.ApprovalStatus,
			&lr.Done,
			&lr.AccountCode,
			&lr.CompanyCode,
			&lr.ModifiedDate,
			&lr.ModifiedBy,
			&lr.CreatedDate,
			&lr.CreatedBy,
		); err != nil {
			utility.Error(err, nil)
			return
		}
		list = append(list, lr)
	}

	if err = rows.Err(); err != nil && err != sql.ErrNoRows {
		utility.Error(err, nil)
		return
	}
	return
}

func (e *RequestApprovalRepo) Insert(ctx context.Context, tx *sql.Tx, requestApproval *entity.RequestApproval) (err error) {
	query, values, err := utility.SqlDialect.Insert(goqu.T("request_approval")).Rows(requestApproval).Prepared(true).ToSQL()
	if err != nil {
		utility.Error(err, nil)
		return
	}

	if tx == nil {
		tx, err = e.db.BeginTx(ctx, nil)
		if err != nil {
			utility.Error(err, nil)
			return
		}

		defer func() {
			if err != nil {
				tx.Rollback()
				return
			}
			tx.Commit()
		}()
	}

	result, err := tx.ExecContext(ctx, query, values...)
	if err != nil {
		utility.Error(err, nil)
		return
	}

	id, err := result.LastInsertId()
	if err != nil {
		utility.Error(err, nil)
		return
	}

	requestApproval.ID = int(id)
	return
}

func (e *RequestApprovalRepo) Update(ctx context.Context, tx *sql.Tx, requestApproval *entity.RequestApproval) (err error) {
	if tx == nil {
		tx, err = e.db.BeginTx(ctx, nil)
		if err != nil {
			utility.Error(err, nil)
			return
		}

		defer func() {
			if err != nil {
				tx.Rollback()
				return
			}
			tx.Commit()
		}()
	}

	query, values, err := utility.SqlDialect.
		Update(goqu.T("request_approval")).
		Set(goqu.Ex{
			"approval_status": requestApproval.ApprovalStatus,
			"done":            requestApproval.Done,
			"modified_date":   requestApproval.ModifiedDate,
			"modified_by":     requestApproval.ModifiedBy,
		}).
		Where(
			goqu.I("id").Eq(requestApproval.ID),
		).
		Prepared(true).ToSQL()

	if err != nil {
		utility.Error(err, nil)
		return
	}

	_, err = tx.ExecContext(ctx, query, values...)
	if err != nil {
		utility.Error(err, nil)
		return
	}

	return
}
