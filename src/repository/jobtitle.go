package repository

import (
	"context"
	"database/sql"
	"mekr/src/entity"
	"mekr/src/utility"

	"github.com/doug-martin/goqu/v9"
)

type JobtitleRepo struct {
	db *sql.DB
}

func InitJobtitleRepo(db *sql.DB) JobtitleRepoIface {
	return &JobtitleRepo{
		db: db,
	}
}

type JobtitleRepoIface interface {
	DetailJobtitle(ctx context.Context, jobtitleCode, accountCode, companyCode string) (jobtitle *entity.Jobtitle, err error)
}

func (j *JobtitleRepo) DetailJobtitle(ctx context.Context, jobtitleCode, accountCode, companyCode string) (jobtitle *entity.Jobtitle, err error) {
	dataset := utility.SqlDialect.Select(
		goqu.I("j.jobtitle_code"),
		goqu.I("j.jobtitle_name"),
		goqu.I("j.description"),
		goqu.I("j.account_code"),
		goqu.I("j.company_code"),
		goqu.I("j.modified_date"),
		goqu.I("j.modified_by"),
		goqu.I("j.created_date"),
		goqu.I("j.created_by"),
	).
		From(goqu.T("jobtitle").As("j")).
		Where(
			goqu.I("j.account_code").Eq(accountCode),
			goqu.I("j.company_code").Eq(companyCode),
		)

	if jobtitleCode != "" {
		dataset = dataset.Where(
			goqu.I("j.jobtitle_code").Eq(jobtitleCode),
		)
	}

	query, params, err := dataset.Prepared(true).ToSQL()
	if err != nil {
		utility.Error(err, nil)
		return
	}

	jobtitle = &entity.Jobtitle{}
	err = j.db.QueryRow(query, params...).
		Scan(
			&jobtitle.JobtitleCode,
			&jobtitle.JobtitleName,
			&jobtitle.Description,
			&jobtitle.AccountCode,
			&jobtitle.CompanyCode,
			&jobtitle.ModifiedDate,
			&jobtitle.ModifiedBy,
			&jobtitle.CreatedDate,
			&jobtitle.CreatedBy,
		)
	if err != nil && err != sql.ErrNoRows {
		utility.Error(err, nil)
		return
	}
	err = nil
	return
}
