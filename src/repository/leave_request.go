package repository

import (
	"context"
	"database/sql"
	"mekr/src/entity"
	"mekr/src/utility"

	"github.com/doug-martin/goqu/v9"
)

type LeaveRequestRepo struct {
	db *sql.DB
}

func InitLeaveRequestRepo(db *sql.DB) LeaveRequestRepoIface {
	return &LeaveRequestRepo{
		db: db,
	}
}

type LeaveRequestRepoIface interface {
	List(ctx context.Context, param ParamListLeaveRequest) (list []entity.LeaveRequest, err error)
	Insert(ctx context.Context, tx *sql.Tx, leaveRequest *entity.LeaveRequest) (err error)
	Update(ctx context.Context, tx *sql.Tx, leaveRequest *entity.LeaveRequest) (err error)

	BeginTx(context.Context) (*sql.Tx, error)
}

type ParamListLeaveRequest struct {
	LeaveRequestID int
	LeaveRequestNo string
	EmpID          int
	ApproverID     int
	AccountCode    string
	CompanyCode    string
	Pagin          utility.PaginParam
}

func (e *LeaveRequestRepo) List(ctx context.Context, param ParamListLeaveRequest) (list []entity.LeaveRequest, err error) {
	dataset := utility.SqlDialect.
		Select(
			goqu.I("lr.id"),
			goqu.I("lr.leave_request_no"),
			goqu.I("lr.employee_id"),
			goqu.I("e.employee_name"),
			goqu.I("lr.leave_start"),
			goqu.I("lr.leave_end"),
			goqu.I("lr.request_status"),
			goqu.I("lr.leave_reason"),
			goqu.I("lr.leave_type"),
			goqu.I("lr.account_code"),
			goqu.I("lr.company_code"),
			goqu.I("lr.modified_date"),
			goqu.I("lr.modified_by"),
			goqu.I("lr.created_date"),
			goqu.I("lr.created_by"),
		).
		From(goqu.T("leave_request").As("lr")).
		LeftJoin(goqu.T("employee").As("e"),
			goqu.On(
				goqu.I("e.id").Eq(goqu.I("lr.employee_id")),
				goqu.I("e.account_code").Eq(goqu.I("lr.account_code")),
				goqu.I("e.company_code").Eq(goqu.I("lr.company_code")),
			),
		).
		Where(
			goqu.I("lr.account_code").Eq(param.AccountCode),
			goqu.I("lr.company_code").Eq(param.CompanyCode),
		).
		Order(
			goqu.I("e.id").Asc(),
		)

	if param.LeaveRequestID > 0 {
		dataset = dataset.Where(
			goqu.I("lr.id").Eq(param.LeaveRequestID),
		)
	}

	if param.LeaveRequestNo != "" {
		dataset = dataset.Where(
			goqu.I("lr.leave_request_no").Eq(param.LeaveRequestNo),
		)
	}

	if param.EmpID > 0 && param.ApproverID > 0 {
		dataset = dataset.LeftJoin(goqu.T("request_approval").As("ra"), goqu.On(
			goqu.I("ra.request_id").Eq(goqu.I("lr.id")),
		))
		dataset = dataset.Where(
			goqu.Or(
				goqu.I("ra.approver_emp_id").Eq(param.ApproverID),
				goqu.I("lr.employee_id").Eq(param.EmpID),
			),
		)
	} else if param.ApproverID > 0 {
		dataset = dataset.Join(goqu.T("request_approval").As("ra"), goqu.On(
			goqu.I("ra.request_id").Eq(goqu.I("lr.id")),
		))
		dataset = dataset.Where(
			goqu.I("ra.approver_emp_id").Eq(param.ApproverID),
		)
	} else if param.EmpID > 0 {
		dataset = dataset.Where(
			goqu.I("lr.employee_id").Eq(param.EmpID),
		)
	}

	query, params, err := dataset.Prepared(true).ToSQL()
	if err != nil {
		utility.Error(err, nil)
		return
	}

	rows, err := e.db.QueryContext(ctx, query, params...)
	if err != nil && err != sql.ErrNoRows {
		utility.Error(err, nil)
		return
	}
	defer rows.Close()

	for rows.Next() {
		var lr entity.LeaveRequest
		if err = rows.Scan(
			&lr.ID,
			&lr.LeaveReqNo,
			&lr.EmpID,
			&lr.EmpName,
			&lr.LeaveStart,
			&lr.LeaveEnd,
			&lr.RequestStatus,
			&lr.LeaveReason,
			&lr.LeaveType,
			&lr.AccountCode,
			&lr.CompanyCode,
			&lr.ModifiedDate,
			&lr.ModifiedBy,
			&lr.CreatedDate,
			&lr.CreatedBy,
		); err != nil {
			utility.Error(err, nil)
			return
		}
		list = append(list, lr)
	}

	if err = rows.Err(); err != nil && err != sql.ErrNoRows {
		utility.Error(err, nil)
		return
	}
	return
}

func (e *LeaveRequestRepo) Insert(ctx context.Context, tx *sql.Tx, leaveRequest *entity.LeaveRequest) (err error) {
	query, values, err := utility.SqlDialect.Insert(goqu.T("leave_request")).Rows(leaveRequest).Prepared(true).ToSQL()
	if err != nil {
		utility.Error(err, nil)
		return
	}

	if tx == nil {
		tx, err = e.db.BeginTx(ctx, nil)
		if err != nil {
			utility.Error(err, nil)
			return
		}

		defer func() {
			if err != nil {
				tx.Rollback()
				return
			}
			tx.Commit()
		}()
	}

	result, err := tx.ExecContext(ctx, query, values...)
	if err != nil {
		utility.Error(err, nil)
		return
	}

	id, err := result.LastInsertId()
	if err != nil {
		utility.Error(err, nil)
		return
	}

	leaveRequest.ID = int(id)
	// invoice.InvoiceNo = id

	// err = e.Update(ctx, tx, emp)
	// if err != nil {
	// 	util.Error(err, nil)
	// 	return
	// }

	return
}

func (e *LeaveRequestRepo) Update(ctx context.Context, tx *sql.Tx, leaveRequest *entity.LeaveRequest) (err error) {
	if tx == nil {
		tx, err = e.db.BeginTx(ctx, nil)
		if err != nil {
			utility.Error(err, nil)
			return
		}

		defer func() {
			if err != nil {
				tx.Rollback()
				return
			}
			tx.Commit()
		}()
	}

	query, values, err := utility.SqlDialect.
		Update(goqu.T("leave_request")).
		Set(goqu.Ex{
			"leave_request_no": leaveRequest.LeaveReqNo,
			"request_status":   leaveRequest.RequestStatus,
			"modified_date":    leaveRequest.ModifiedDate,
			"modified_by":      leaveRequest.ModifiedBy,
		}).
		Where(
			goqu.I("id").Eq(leaveRequest.ID),
		).
		Prepared(true).ToSQL()

	if err != nil {
		utility.Error(err, nil)
		return
	}

	_, err = tx.ExecContext(ctx, query, values...)
	if err != nil {
		utility.Error(err, nil)
		return
	}

	return
}

func (i *LeaveRequestRepo) BeginTx(ctx context.Context) (*sql.Tx, error) {
	tx, err := i.db.BeginTx(ctx, nil)
	if err != nil {
		utility.Error(err, nil)
	}
	return tx, err
}
