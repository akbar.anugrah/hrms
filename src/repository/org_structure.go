package repository

import (
	"context"
	"database/sql"
	"mekr/src/entity"
	"mekr/src/utility"

	"github.com/doug-martin/goqu/v9"
)

type OrgStructureRepo struct {
	db *sql.DB
}

func InitOrgStructureRepo(db *sql.DB) OrgStructureRepoIface {
	return &OrgStructureRepo{
		db: db,
	}
}

type OrgStructureRepoIface interface {
	DetailOrgStructure(ctx context.Context, childEmpID, parentEmpID int, accountCode, companyCode string) (orgStructure *entity.OrgStructure, err error)
}

func (o *OrgStructureRepo) DetailOrgStructure(ctx context.Context, childEmpID, parentEmpID int, accountCode, companyCode string) (orgStructure *entity.OrgStructure, err error) {
	dataset := utility.SqlDialect.Select(
		goqu.I("o.id"),
		goqu.I("o.parent_emp_id"),
		goqu.I("o.child_emp_id"),
		goqu.I("o.account_code"),
		goqu.I("o.company_code"),
		goqu.I("o.modified_date"),
		goqu.I("o.modified_by"),
		goqu.I("o.created_date"),
		goqu.I("o.created_by"),
	).
		From(goqu.T("org_structure").As("o")).
		Where(
			goqu.I("o.account_code").Eq(accountCode),
			goqu.I("o.company_code").Eq(companyCode),
		)

	if childEmpID > 0 {
		dataset = dataset.Where(
			goqu.I("o.child_emp_id").Eq(childEmpID),
		)
	}

	if parentEmpID > 0 {
		dataset = dataset.Where(
			goqu.I("o.parent_emp_id").Eq(parentEmpID),
		)
	}

	query, params, err := dataset.Prepared(true).ToSQL()
	if err != nil {
		utility.Error(err, nil)
		return
	}

	orgStructure = &entity.OrgStructure{}
	err = o.db.QueryRow(query, params...).
		Scan(
			&orgStructure.ID,
			&orgStructure.ParentEmpID,
			&orgStructure.ChildEmpID,
			&orgStructure.AccountCode,
			&orgStructure.CompanyCode,
			&orgStructure.ModifiedDate,
			&orgStructure.ModifiedBy,
			&orgStructure.CreatedDate,
			&orgStructure.CreatedBy,
		)
	if err != nil && err != sql.ErrNoRows {
		utility.Error(err, nil)
		return
	}
	err = nil
	return
}
