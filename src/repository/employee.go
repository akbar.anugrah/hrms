package repository

import (
	"context"
	"database/sql"
	"mekr/src/entity"
	"mekr/src/utility"

	"github.com/doug-martin/goqu/v9"
)

type EmployeeRepo struct {
	db *sql.DB
}

func InitEmployeeRepo(db *sql.DB) EmployeeRepoIface {
	return &EmployeeRepo{
		db: db,
	}
}

type EmployeeRepoIface interface {
	DetailEmployee(ctx context.Context, empID int, username string) (emp *entity.Employee, err error)
	List(ctx context.Context, Pagin utility.PaginParam) (list []entity.Employee, err error)
	Insert(ctx context.Context, tx *sql.Tx, emp *entity.Employee) (err error)
	Update(ctx context.Context, tx *sql.Tx, emp *entity.Employee) (err error)
	Delete(ctx context.Context, empID int) (err error)
}

var selectEmpDataselect = utility.SqlDialect.
	Select(
		goqu.I("e.id"),
		goqu.I("e.employee_no"),
		goqu.I("e.employee_name"),
		goqu.I("e.emp_type"),
		goqu.I("e.sex"),
		goqu.I("e.email"),
		goqu.I("e.username"),
		goqu.I("e.password"),
		goqu.I("e.jobtitle_code"),
		goqu.I("j.jobtitle_name"),
		goqu.I("e.account_code"),
		goqu.I("e.company_code"),
		goqu.I("e.modified_date"),
		goqu.I("e.modified_by"),
		goqu.I("e.created_date"),
		goqu.I("e.created_by"),
	).
	From(goqu.T("employee").As("e")).
	LeftJoin(goqu.T("jobtitle").As("j"),
		goqu.On(
			goqu.I("j.jobtitle_code").Eq(goqu.I("e.jobtitle_code")),
			goqu.I("j.account_code").Eq(goqu.I("e.account_code")),
			goqu.I("j.company_code").Eq(goqu.I("e.company_code")),
		),
	).
	Order(
		goqu.I("e.employee_name").Asc(),
	)

func (e *EmployeeRepo) DetailEmployee(ctx context.Context, empID int, username string) (emp *entity.Employee, err error) {

	dataset := selectEmpDataselect

	if empID != 0 {
		dataset = dataset.Where(
			goqu.I("e.id").Eq(empID),
		)
	}

	if username != "" {
		dataset = dataset.Where(
			goqu.I("e.username").Eq(username),
		)
	}

	query, params, err := dataset.Prepared(true).ToSQL()
	if err != nil {
		utility.Error(err, nil)
		return
	}

	emp = &entity.Employee{}
	err = e.db.QueryRow(query, params...).
		Scan(
			&emp.ID,
			&emp.EmpNo,
			&emp.EmpName,
			&emp.EmpType,
			&emp.Sex,
			&emp.Email,
			&emp.Username,
			&emp.Password,
			&emp.JobtitleCode,
			&emp.JobtitleName,
			&emp.AccountCode,
			&emp.CompanyCode,
			&emp.ModifiedDate,
			&emp.ModifiedBy,
			&emp.CreatedDate,
			&emp.CreatedBy,
		)
	if err != nil && err != sql.ErrNoRows {
		utility.Error(err, nil)
		return
	}
	err = nil
	return
}

func (e *EmployeeRepo) List(ctx context.Context, Pagin utility.PaginParam) (list []entity.Employee, err error) {

	dataset := selectEmpDataselect.
		Limit(Pagin.GetLimit()).
		Offset(Pagin.GetOffset())

	query, params, err := dataset.Prepared(true).ToSQL()
	if err != nil {
		utility.Error(err, nil)
		return
	}

	rows, err := e.db.QueryContext(ctx, query, params...)
	if err != nil && err != sql.ErrNoRows {
		utility.Error(err, nil)
		return
	}
	defer rows.Close()

	for rows.Next() {
		var emp entity.Employee
		if err = rows.Scan(
			&emp.ID,
			&emp.EmpNo,
			&emp.EmpName,
			&emp.EmpType,
			&emp.Sex,
			&emp.Email,
			&emp.Username,
			&emp.Password,
			&emp.JobtitleCode,
			&emp.JobtitleName,
			&emp.AccountCode,
			&emp.CompanyCode,
			&emp.ModifiedDate,
			&emp.ModifiedBy,
			&emp.CreatedDate,
			&emp.CreatedBy,
		); err != nil {
			utility.Error(err, nil)
			return
		}
		list = append(list, emp)
	}

	if err = rows.Err(); err != nil && err != sql.ErrNoRows {
		utility.Error(err, nil)
		return
	}
	return
}

func (e *EmployeeRepo) Insert(ctx context.Context, tx *sql.Tx, emp *entity.Employee) (err error) {
	query, values, err := utility.SqlDialect.Insert(goqu.T("employee")).Rows(emp).Prepared(true).ToSQL()
	if err != nil {
		utility.Error(err, nil)
		return
	}

	if tx == nil {
		tx, err = e.db.BeginTx(ctx, nil)
		if err != nil {
			utility.Error(err, nil)
			return
		}

		defer func() {
			if err != nil {
				tx.Rollback()
				return
			}
			tx.Commit()
		}()
	}

	result, err := tx.ExecContext(ctx, query, values...)
	if err != nil {
		utility.Error(err, nil)
		return
	}

	id, err := result.LastInsertId()
	if err != nil {
		utility.Error(err, nil)
		return
	}

	emp.ID = int(id)

	return
}

func (e *EmployeeRepo) Update(ctx context.Context, tx *sql.Tx, emp *entity.Employee) (err error) {
	if tx == nil {
		tx, err = e.db.BeginTx(ctx, nil)
		if err != nil {
			utility.Error(err, nil)
			return
		}

		defer func() {
			if err != nil {
				tx.Rollback()
				return
			}
			tx.Commit()
		}()
	}

	query, values, err := utility.SqlDialect.
		Update(goqu.T("employee")).
		Set(goqu.Ex{
			"employee_no":   emp.EmpNo,
			"employee_name": emp.EmpName,
			"emp_type":      emp.EmpType,
			"email":         emp.Email,
			"sex":           emp.Sex,
			"jobtitle_code": emp.JobtitleCode,
			"modified_date": emp.ModifiedDate,
			"modified_by":   emp.ModifiedBy,
		}).
		Where(
			goqu.I("id").Eq(emp.ID),
		).
		Prepared(true).ToSQL()

	if err != nil {
		utility.Error(err, nil)
		return
	}

	_, err = tx.ExecContext(ctx, query, values...)
	if err != nil {
		utility.Error(err, nil)
		return
	}

	return
}

func (e *EmployeeRepo) Delete(ctx context.Context, empID int) (err error) {
	query, value, err := utility.SqlDialect.Delete(goqu.T("employee")).
		Where(goqu.Ex{
			"id": empID,
		}).Prepared(true).ToSQL()
	if err != nil {
		utility.Error(err, nil)
		return err
	}

	_, err = e.db.ExecContext(ctx, query, value...)
	if err != nil {
		utility.Error(err, nil)
		return err
	}
	return nil
}
