package entity

import "time"

type OrgStructure struct {
	ID           int       `db:"id"`
	ParentEmpID  *int      `db:"parent_emp_id"`
	ChildEmpID   *int      `db:"child_emp_id"`
	CompanyCode  string    `db:"company_code"`
	AccountCode  string    `db:"account_code"`
	ModifiedDate time.Time `db:"modified_date"`
	ModifiedBy   string    `db:"modified_by"`
	CreatedDate  time.Time `db:"created_date"`
	CreatedBy    string    `db:"created_by"`
}
