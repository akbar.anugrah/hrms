package entity

import "time"

type Jobtitle struct {
	JobtitleCode string    `db:"jobtitle_code"`
	JobtitleName string    `db:"jobtitle_name"`
	Description  string    `db:"description"`
	AccountCode  string    `db:"account_code"`
	CompanyCode  string    `db:"company_code"`
	CreatedBy    string    `db:"created_by"`
	CreatedDate  time.Time `db:"created_date"`
	ModifiedBy   string    `db:"modified_by"`
	ModifiedDate time.Time `db:"modified_date"`
}
