package entity

import (
	"log"
	"mekr/src/utility"
	"strconv"
	"time"
)

const (
	LeaveAnnualQuota int = 12

	LeaveTypeAnnual int = 1
	LeaveTypeSick   int = 2
)

type LeaveRequest struct {
	ID            int       `db:"id"`
	LeaveReqNo    string    `db:"leave_request_no"`
	CompanyCode   string    `db:"company_code"`
	AccountCode   string    `db:"account_code"`
	EmpID         int       `db:"employee_id"`
	EmpName       string    `db:"employee_name" goqu:"skipinsert,skipupdate"`
	LeaveStart    time.Time `db:"leave_start"`
	LeaveEnd      time.Time `db:"leave_end"`
	RequestStatus int       `db:"request_status"`
	LeaveReason   string    `db:"leave_reason"`
	LeaveType     int       `db:"leave_type"`
	ModifiedDate  time.Time `db:"modified_date"`
	ModifiedBy    string    `db:"modified_by"`
	CreatedDate   time.Time `db:"created_date"`
	CreatedBy     string    `db:"created_by"`
}

func CreateLeaveRequest(
	leaveReq *LeaveRequest,
	actor *Employee,
	prevRequests []LeaveRequest) (leaveRequest *LeaveRequest, err error) {
	if leaveReq == nil {
		err = utility.MissingRequiredParam
		return
	}
	if actor == nil {
		err = utility.Unathorized
		return
	}
	if !EmpHasLeaveQuota(leaveReq, prevRequests) {
		err = utility.InsufficientLeaveBalance
		return
	}
	leaveRequest = leaveReq
	if err = leaveRequest.ValidateInputInsert(); err != nil {
		return
	}
	leaveRequest.EmpID = actor.ID
	leaveRequest.RequestStatus = ApprovalUnverified
	leaveRequest.AccountCode = actor.AccountCode
	leaveRequest.CompanyCode = actor.CompanyCode
	leaveRequest.ModifiedBy = actor.Username
	leaveRequest.ModifiedDate = time.Now()
	leaveRequest.CreatedBy = actor.Username
	leaveRequest.CreatedDate = time.Now()
	return
}

func (l *LeaveRequest) GenerateLeaveReqNo() {
	l.LeaveReqNo = "LVREQ-" + strconv.Itoa(l.EmpID) + "-" + strconv.Itoa(l.ID)
}

func (l *LeaveRequest) GetRequestStatusString() string {
	switch l.RequestStatus {
	case ApprovalUnverified:
		return "unverified"
	case ApprovalApproved:
		return "approved"
	case ApprovalFullyApproved:
		return "fully_approved"
	case ApprovalRejected:
		return "rejected"
	default:
		return "unverified"
	}
}

func (l *LeaveRequest) GetLeaveTypeString() string {
	switch l.LeaveType {
	case 1:
		return "annual_leave"
	default:
		return "annual_leave"
	}
}

func (l *LeaveRequest) ValidateInputInsert() (err error) {
	if l.LeaveReason == "" ||
		l.LeaveType == 0 ||
		l.LeaveStart.IsZero() ||
		l.LeaveEnd.IsZero() ||
		l.LeaveStart.After(l.LeaveEnd) {
		log.Printf("%+v\n", *l)
		err = utility.MissingRequiredParam
	}
	return
}

func EmpHasLeaveQuota(leaveReq *LeaveRequest, prevRequest []LeaveRequest) bool {
	var leaveTaken float64
	for _, pReq := range prevRequest {
		// only count previous leave from the same year of request
		if pReq.EmpID == leaveReq.EmpID && pReq.CreatedDate.Year() == leaveReq.CreatedDate.Year() {
			leaveTaken += pReq.LeaveEnd.Sub(pReq.LeaveStart).Hours() / 24
		}
	}
	inputReqDuration := leaveReq.LeaveEnd.Sub(leaveReq.LeaveStart).Hours() / 24
	return inputReqDuration+leaveTaken <= float64(LeaveAnnualQuota)
}

func (l *LeaveRequest) UpdateStatus(inputStatus int, actor *Employee, requestApproval []RequestApproval) (updateReqApproval RequestApproval, err error) {
	if l.EmpID == 0 || actor == nil || actor.ID == 0 || requestApproval == nil {
		err = utility.Unathorized
		return
	}

	if !ValidRequestApprovalStatus(inputStatus) {
		err = utility.InvalidRequestStatus
		return
	}

	totalApproved := 0
	totalApprover := len(requestApproval)
	reject := false

	// reqApproval := RequestApproval{}
	for _, rAppr := range requestApproval {
		if rAppr.ApproverEmpID == actor.ID {
			rAppr.ApprovalStatus = inputStatus
			updateReqApproval = rAppr
			updateReqApproval.Done = 1
			updateReqApproval.ModifiedBy = actor.Username
			updateReqApproval.ModifiedDate = time.Now()
		}
		if rAppr.ApprovalStatus == ApprovalApproved {
			totalApproved++
		}
		if rAppr.ApprovalStatus == ApprovalRejected {
			reject = true
		}
	}

	if totalApproved == totalApprover {
		l.RequestStatus = ApprovalFullyApproved
	}
	if totalApproved < totalApprover {
		if reject {
			l.RequestStatus = ApprovalRejected
		} else if totalApproved == 0 {
			l.RequestStatus = ApprovalUnverified
		} else {
			l.RequestStatus = ApprovalApproved
		}
	}

	l.ModifiedBy = actor.Username
	l.ModifiedDate = time.Now()
	return
}
