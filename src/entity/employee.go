package entity

import (
	"mekr/src/utility"
	"time"
)

type Employee struct {
	ID           int       `db:"id"`
	EmpNo        string    `db:"employee_no"`
	EmpName      string    `db:"employee_name"`
	EmpType      int       `db:"emp_type"` // '3' for Admin, `1` for regular employee
	Sex          string    `db:"sex"`
	Email        string    `db:"email"`
	Username     string    `db:"username"`
	Password     string    `db:"password"`
	JobtitleCode string    `db:"jobtitle_code"`
	JobtitleName string    `db:"jobtitle_name" goqu:"skipinsert,skipupdate"`
	AccountCode  string    `db:"account_code"`
	CompanyCode  string    `db:"company_code"`
	ModifiedDate time.Time `db:"modified_date"`
	ModifiedBy   string    `db:"modified_by"`
	CreatedDate  time.Time `db:"created_date"`
	CreatedBy    string    `db:"created_by"`
}

func (e *Employee) IsAdmin(adminOf *Employee) bool {
	if adminOf != nil {
		return e.AccountCode == adminOf.AccountCode &&
			e.CompanyCode == adminOf.CompanyCode &&
			e.EmpType == 1
	}
	return e.EmpType == 1
}

func (e *Employee) ValidateInputInsert() error {
	if e.EmpNo == "" ||
		e.EmpName == "" ||
		e.Sex == "" ||
		(e.Sex != "m" && e.Sex != "f") ||
		e.Email == "" ||
		e.Password == "" ||
		e.JobtitleCode == "" ||
		e.AccountCode == "" ||
		e.CompanyCode == "" {
		return utility.MissingRequiredParam
	}
	return nil
}

func CreateEmployee(data, actor *Employee, jobtitle *Jobtitle, duplicateEmp *Employee) (emp *Employee, err error) {
	if data == nil {
		err = utility.MissingRequiredParam
		return
	}

	if actor == nil || !actor.IsAdmin(nil) {
		err = utility.Unathorized
		return
	}

	if duplicateEmp != nil && duplicateEmp.Username == data.Username {
		err = utility.DuplicateUsername
		return
	}

	if jobtitle.JobtitleCode == "" {
		err = utility.MissingRequiredParam
		return
	}

	emp = data
	emp.AccountCode = actor.AccountCode
	emp.CompanyCode = actor.CompanyCode
	emp.Password = utility.GetMD5Hash(data.Password + data.Username)
	emp.CreatedBy = actor.Username
	emp.CreatedDate = time.Now()
	emp.ModifiedBy = actor.Username
	emp.ModifiedDate = time.Now()

	if err = emp.ValidateInputInsert(); err != nil {
		return
	}

	return
}

func (e *Employee) ValidateInputUpdate() error {
	if e.EmpNo == "" ||
		e.EmpName == "" ||
		e.EmpType == 0 ||
		e.Sex == "" ||
		(e.Sex != "m" && e.Sex != "f") ||
		e.Email == "" ||
		e.JobtitleCode == "" {
		return utility.MissingRequiredParam
	}
	return nil
}

func (e *Employee) Update(data, actor *Employee, jobtitle *Jobtitle) (err error) {
	if data == nil {
		err = utility.MissingRequiredParam
		return
	}

	if actor == nil || !actor.IsAdmin(e) {
		err = utility.Unathorized
		return
	}

	if jobtitle.JobtitleCode == "" {
		err = utility.MissingRequiredParam
		return
	}

	if err = data.ValidateInputUpdate(); err != nil {
		return
	}

	e.EmpNo = data.EmpNo
	e.EmpName = data.EmpName
	e.EmpType = data.EmpType
	e.Sex = data.Sex
	e.Email = data.Email
	e.JobtitleCode = data.JobtitleCode
	e.ModifiedBy = actor.Username
	e.ModifiedDate = time.Now()

	return
}
