package entity

import "time"

const (
	ApprovalUnverified    int = 0
	ApprovalApproved      int = 1
	ApprovalFullyApproved int = 2
	ApprovalRejected      int = 3

	RequestTypeLeave int = 1
)

type RequestApproval struct {
	ID              int       `db:"id"`
	RequestID       int       `db:"request_id"`
	RequestType     string    `db:"request_type"`
	AccountCode     string    `db:"account_code"`
	CompanyCode     string    `db:"company_code"`
	ApproverEmpID   int       `db:"approver_emp_id"`
	ApproverEmpName string    `db:"approver_emp_name" goqu:"skipinsert,skipupdate"`
	ApprovalStatus  int       `db:"approval_status"`
	Done            int       `db:"done"`
	Sequence        int       `db:"sequence"`
	ModifiedDate    time.Time `db:"modified_date"`
	ModifiedBy      string    `db:"modified_by"`
	CreatedDate     time.Time `db:"created_date"`
	CreatedBy       string    `db:"created_by"`
}

func CreateRequestApproval(actor *Employee, request interface{}, superior []OrgStructure) (reqAppr []RequestApproval) {
	requestID := 0
	if req, ok := request.(*LeaveRequest); ok {
		requestID = req.ID
	}
	sequence := 0
	for _, super := range superior {
		reqAppr = append(reqAppr, RequestApproval{
			RequestID:      requestID,
			RequestType:    "LEAVE",
			Sequence:       sequence,
			AccountCode:    actor.AccountCode,
			CompanyCode:    actor.CompanyCode,
			ApproverEmpID:  *super.ParentEmpID,
			ApprovalStatus: ApprovalUnverified,
			Done:           0,
			ModifiedDate:   time.Now(),
			ModifiedBy:     actor.Username,
			CreatedDate:    time.Now(),
			CreatedBy:      actor.Username,
		})
		sequence++
	}
	return
}

func ValidRequestApprovalStatus(apprStatus int) bool {
	return apprStatus == ApprovalApproved ||
		apprStatus == ApprovalRejected
}

func (ra *RequestApproval) GetApprovalStatusString() string {
	switch ra.ApprovalStatus {
	case ApprovalUnverified:
		return "unverified"
	case ApprovalApproved:
		return "approved"
	case ApprovalFullyApproved:
		return "fully_approved"
	case ApprovalRejected:
		return "rejected"
	default:
		return "unverified"
	}
}
