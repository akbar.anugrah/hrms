package api

import (
	"mekr/src/entity"
)

type CreateEmployeeReq struct {
	EmployeeNo   string `json:"employee_no"`
	EmployeeName string `json:"employee_name"`
	EmployeeType int    `json:"employee_type"`
	JobtitleCode string `json:"jobtitle_code"`
	Sex          string `json:"sex"`
	Email        string `json:"email"`
	Username     string `json:"username"`
	Password     string `json:"password"`
}

type UpdateEmployeeReq struct {
	ID           int    `json:"employee_id"`
	EmployeeNo   string `json:"employee_no"`
	EmployeeName string `json:"employee_name"`
	EmployeeType int    `json:"employee_type"`
	JobtitleCode string `json:"jobtitle_code"`
	Sex          string `json:"sex"`
	Email        string `json:"email"`
}

func (c *CreateEmployeeReq) ToEntity() *entity.Employee {
	return &entity.Employee{
		EmpNo:        c.EmployeeNo,
		EmpName:      c.EmployeeName,
		EmpType:      c.EmployeeType,
		Sex:          c.Sex,
		Email:        c.Email,
		Username:     c.Username,
		Password:     c.Password,
		JobtitleCode: c.JobtitleCode,
	}
}

func (c *UpdateEmployeeReq) ToEntity() *entity.Employee {
	return &entity.Employee{
		ID:           c.ID,
		EmpNo:        c.EmployeeNo,
		EmpName:      c.EmployeeName,
		EmpType:      c.EmployeeType,
		Sex:          c.Sex,
		Email:        c.Email,
		JobtitleCode: c.JobtitleCode,
	}
}

type ListEmployee struct {
	ID           int    `json:"employee_id"`
	EmployeeNo   string `json:"employee_no"`
	EmployeeName string `json:"employee_name"`
	EmployeeType int    `json:"employee_type"`
	JobtitleCode string `json:"jobtitle_code"`
	Sex          string `json:"sex"`
	Email        string `json:"email"`
	Username     string `json:"username"`
}

func GetEmployee(emp entity.Employee) (e map[string]interface{}) {
	e = make(map[string]interface{})
	e["employee_id"] = emp.ID
	e["employee_no"] = emp.EmpNo
	e["employee_name"] = emp.EmpName
	e["employee_type"] = emp.EmpType
	e["jobtitle_code"] = emp.JobtitleCode
	e["sex"] = emp.Sex
	e["email"] = emp.Email
	e["username"] = emp.Username
	return
}

type DeleteEmployeeReq struct {
	ID int `json:"employee_id"`
}
