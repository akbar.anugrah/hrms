package api

import (
	"mekr/src/entity"
	"time"
)

type CreateLeaveRequest struct {
	LeaveStart  string `json:"LEAVE_START"`
	LeaveEnd    string `json:"LEAVE_END"`
	LeaveReason string `json:"LEAVE_REASON"`
	LeaveType   int    `json:"LEAVE_TYPE"`
}

func (c *CreateLeaveRequest) ToEntity() (lReq *entity.LeaveRequest) {
	lReq = &entity.LeaveRequest{}
	lReq.LeaveStart, _ = time.Parse("2006-01-02", c.LeaveStart)
	lReq.LeaveEnd, _ = time.Parse("2006-01-02", c.LeaveEnd)
	lReq.LeaveReason = c.LeaveReason
	lReq.LeaveType = c.LeaveType
	return lReq
}

type GetLeaveRequest struct {
	ID              int                  `json:"ID"`
	LeaveRequestNo  string               `json:"leave_request_no"`
	EmployeeID      int                  `json:"employee_id"`
	EmployeeName    string               `json:"employee_name"`
	LeaveStart      string               `json:"leave_start"`
	LeaveEnd        string               `json:"leave_end"`
	RequestStatus   string               `json:"request_status"`
	LeaveReason     string               `json:"leave_reason"`
	LeaveType       string               `json:"leave_type"`
	CreatedDate     time.Time            `json:"created_date"`
	RequestApproval []GetRequestApproval `json:"request_approvals"`
}

type GetRequestApproval struct {
	ID              int    `json:"ID"`
	ApproverEmpID   int    `json:"approver_emp_id"`
	ApproverEmpName string `json:"approver_emp_name"`
	ApprovalStatus  string `json:"approval_status"`
	Done            int    `json:"done"`
}

func ListLeaveRequests(leaveRequest *entity.LeaveRequest, requestApprovals []entity.RequestApproval) *GetLeaveRequest {
	getReqApprovals := []GetRequestApproval{}
	for _, reqAppr := range requestApprovals {
		getReqApprovals = append(getReqApprovals, GetRequestApproval{
			ID:              reqAppr.ID,
			ApproverEmpID:   reqAppr.ApproverEmpID,
			ApproverEmpName: reqAppr.ApproverEmpName,
			ApprovalStatus:  reqAppr.GetApprovalStatusString(),
			Done:            reqAppr.Done,
		})
	}
	return &GetLeaveRequest{
		ID:              leaveRequest.ID,
		LeaveRequestNo:  leaveRequest.LeaveReqNo,
		EmployeeID:      leaveRequest.EmpID,
		EmployeeName:    leaveRequest.EmpName,
		LeaveStart:      leaveRequest.LeaveStart.Format("2006-01-01"),
		LeaveEnd:        leaveRequest.LeaveEnd.Format("2006-01-01"),
		RequestStatus:   leaveRequest.GetRequestStatusString(),
		LeaveReason:     leaveRequest.LeaveReason,
		LeaveType:       leaveRequest.GetLeaveTypeString(),
		CreatedDate:     leaveRequest.CreatedDate,
		RequestApproval: getReqApprovals,
	}
}

type UpdateLeaveRequest struct {
	LeaveRequestNo string `json:"LEAVE_REQUEST_NO"`
	ApprovalStatus int    `json:"APPROVAL_STATUS"`
}

// type
