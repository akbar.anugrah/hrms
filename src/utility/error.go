package utility

import (
	"errors"
)

type ErrorMessage error

var (
	Unathorized          ErrorMessage = errors.New("You are not authorized to access this resource.")
	InternalServerError  error        = errors.New("internal server error")
	MissingRequiredParam ErrorMessage = errors.New("Missing required parameters.")
	ForbiddenToUpdate    string       = "You are forbidden to update '%v'"
	SuccessRetrieveData  string       = "successfully retrieve data"
	SuccessSaveData      string       = "successfully saving data"
	SuccessDeleteData    string       = "successfully delete data"

	InsufficientLeaveBalance ErrorMessage = errors.New("You don't have sufficient leave quota.")
	InvalidRequestStatus     ErrorMessage = errors.New("Invalid approval request status")
	CantUpdateFullyApprove   ErrorMessage = errors.New("You are not allowed to change the fully approved request")
	DuplicateUsername        ErrorMessage = errors.New("Username has been taken")
)
