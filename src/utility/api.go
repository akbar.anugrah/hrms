package utility

import (
	"encoding/json"
	"net/http"
)

type Pagination struct {
	Limit   int    `json:"LIMIT"`
	Page    int    `json:"PAGE"`
	OrderBy string `json:"ORDER_BY"`
}

func (p *Pagination) ParseFromBody() (param *PaginParam) {
	return &PaginParam{
		Limit:   uint(p.Limit),
		Page:    uint(p.Page),
		OrderBy: p.OrderBy,
	}
}

const DateFormat = "02/01/2006"

type PaginParam struct {
	Limit   uint
	Page    uint
	OrderBy string
}

func (p *PaginParam) GetLimit() uint {
	if p.Limit == 0 || p.Limit > 100 {
		return 100
	}
	return p.Limit
}

func (p *PaginParam) GetOffset() uint {
	if p.Page < 1 {
		p.Page = 1
	}
	return p.Limit*p.Page - p.Limit
}

func (p *PaginParam) GetOrderBy(def string) string {
	if p.OrderBy == "" {
		return def
	}
	return p.OrderBy
}

func DecodeJsonRequest(r *http.Request, dst interface{}) (err error) {
	return json.NewDecoder(r.Body).Decode(dst)
}

func ResponseError401(w http.ResponseWriter, err error) {
	w.WriteHeader(http.StatusUnauthorized)
	ErrorResponseBody(w, err)
}

func ResponseError400(w http.ResponseWriter, err error) {
	w.WriteHeader(http.StatusBadRequest)
	ErrorResponseBody(w, err)
}

func ResponseError500(w http.ResponseWriter) {
	w.WriteHeader(http.StatusInternalServerError)
	ErrorResponseBody(w, InternalServerError)
}

func ErrorResponseBody(w http.ResponseWriter, err error) {
	json.NewEncoder(w).Encode(ResponseBody{
		Success: false,
		Message: err.Error(),
	})
}

type ResponseBody struct {
	Success bool        `json:"success"`
	Data    interface{} `json:"data"`
	Message string      `json:"message"`
}

func ResponseSuccess200(w http.ResponseWriter, data interface{}, msg string) {
	json.NewEncoder(w).Encode(ResponseBody{
		Success: true,
		Data:    data,
		Message: msg,
	})
}
