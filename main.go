package main

import (
	"mekr/src/controller"
	"mekr/src/repository"
	"mekr/src/utility"

	"github.com/gorilla/mux"
)

func main() {
	utility.InitConfig()
	db := utility.InitDB()

	employeeRepo := repository.InitEmployeeRepo(db)
	jobtitleRepo := repository.InitJobtitleRepo(db)
	leaveReqRepo := repository.InitLeaveRequestRepo(db)
	orgStructRepo := repository.InitOrgStructureRepo(db)
	requestApprovalRepo := repository.InitRequestApprovalRepo(db)

	employeeCtrl := controller.InitEmployeeCtrl(employeeRepo, jobtitleRepo)
	leaveRequestCtrl := controller.InitLeaveRequestCtrl(leaveReqRepo, orgStructRepo, employeeRepo, requestApprovalRepo)

	r := mux.NewRouter().StrictSlash(true)
	r.Use(mux.CORSMethodMiddleware(r))
	r.Use(utility.AuthMiddleware)

	r.HandleFunc("/v1/employees", employeeCtrl.ListEmployee).Methods("GET")
	r.HandleFunc("/v1/employees", employeeCtrl.CreateEmployee).Methods("POST")
	r.HandleFunc("/v1/employees", employeeCtrl.UpdateEmployee).Methods("PUT")
	r.HandleFunc("/v1/employees", employeeCtrl.DeleteEmployee).Methods("DELETE")

	r.HandleFunc("/v1/leave_requests", leaveRequestCtrl.CreateLeaveRequest).Methods("POST")
	r.HandleFunc("/v1/leave_requests", leaveRequestCtrl.ListLeaveRequest).Methods("GET")
	r.HandleFunc("/v1/leave_requests", leaveRequestCtrl.UpdateLeaveRequest).Methods("PUT")

	utility.HttpServer(r)
}
