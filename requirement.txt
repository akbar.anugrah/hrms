Develop a simple Human Resource Management System (HRMS) with the following features:

Database design

Employee Management
- Add new employees./
- List all employees.
- Edit employee details.
- Delete employees.
- Leave Management

Allow employees to apply for leave.
- Form request time off
- Approve or reject leave applications.

chandra.kurniawan@mekari.com

